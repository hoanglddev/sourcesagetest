package hoangld.test.sourcesagetest.Login;

import com.google.gson.JsonObject;

import hoangld.test.sourcesagetest.network.ServerTask;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by hoangle on 2019-10-12.
 */

public class LoginPresenter {

    private LoginView view;

    public LoginPresenter(LoginView view) {
        this.view = view;
    }

    public void login(String email, String password) {
        JsonObject params = new JsonObject();
        params.addProperty("email", email);
        params.addProperty("password", password);
        ServerTask.getInstance().getServices()
                .login(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    view.onLoginSuccess(response);
                }, err -> {
                    err.printStackTrace();
                    view.onLoginFailed(err);
                });
    }

    public void getProfile() {
        ServerTask.getInstance().getServices()
                .getProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    view.onGetProfileSuccess(response);
                }, err -> {
                    err.printStackTrace();
                    view.onGetProfileFailed(err);
                });
    }
}
