package hoangld.test.sourcesagetest.register;

/**
 * Created by hoangle on 2019-10-12.
 */

public interface RegisterView {

    void onRegisterSucess();

    void onRegisterFailed(Throwable err);

}
