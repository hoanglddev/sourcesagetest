package hoangld.test.sourcesagetest.register;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hoangld.test.sourcesagetest.Login.LoginActivity;
import hoangld.test.sourcesagetest.R;
import hoangld.test.sourcesagetest.model.User;

/**
 * Created by hoangle on 2019-10-12.
 */

public class RegisterActivity extends AppCompatActivity implements RegisterView {

    @BindView(R.id.activity_sign_up_ed_name)
    EditText edName;

    @BindView(R.id.activity_sign_up_ed_email)
    EditText edEmail;

    @BindView(R.id.activity_sign_up_ed_age)
    EditText edAge;

    @BindView(R.id.activity_sign_up_ed_password)
    EditText edPassword;

    @BindView(R.id.activity_sign_up_sn_gender)
    Spinner snGender;

    RegisterPresenter registerPresenter;
    int selectedGender;

    private Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        dialog = new ProgressDialog(this);
        ((ProgressDialog) dialog).setMessage("Waiting...");
        dialog.setCancelable(false);

        initGender();
        registerPresenter = new RegisterPresenter(this);
    }

    private void initGender() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.gender));
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        snGender.setAdapter(dataAdapter);
        snGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedGender = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onRegisterSucess() {
        dialog.dismiss();
        Toast.makeText(this, "Register is successfully", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        finish();
    }

    @Override
    public void onRegisterFailed(Throwable err) {
        dialog.dismiss();
        Toast.makeText(this, "Register is failed", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.activity_signup_btn_login)
    public void actionBtnLogin() {
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        finish();
    }

    @OnClick(R.id.activity_sign_up_btn_sign_up)
    public void actionBtnSignUp() {
        if (isDataIsValid()) {
            dialog.show();
            User user = new User();
            user.name = edName.getText().toString();
            user.email = edEmail.getText().toString();
            user.age = Integer.parseInt(edAge.getText().toString());
            user.password = edPassword.getText().toString();
            user.gender = selectedGender;
            registerPresenter.signup(user);
        } else {
            Toast.makeText(getApplicationContext(), "Any field is empty ?", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isDataIsValid() {
        if (TextUtils.isEmpty(edName.getText().toString()))
            return false;

        if (TextUtils.isEmpty(edEmail.getText().toString()))
            return false;

        if (TextUtils.isEmpty(edAge.getText().toString()))
            return false;

        if (snGender.getSelectedItem() == null)
            return false;

        if (TextUtils.isEmpty(edPassword.getText().toString()))
            return false;

        return true;
    }
}
