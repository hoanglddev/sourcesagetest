package hoangld.test.sourcesagetest;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import hoangld.test.sourcesagetest.Login.LoginActivity;
import hoangld.test.sourcesagetest.model.User;
import hoangld.test.sourcesagetest.profile.ProfileFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        getSupportActionBar().setTitle("Profile");
        navigationView.setCheckedItem(R.id.nav_profile);
        replaceContent(new ProfileFragment());

        SharedPreferences preferences = getSharedPreferences(User.USER_PREF, MODE_PRIVATE);

        View header = navigationView.getHeaderView(0);

        TextView tvName = header.findViewById(R.id.nav_header_home_tv_name);
        TextView tvGender = header.findViewById(R.id.nav_header_main_tv_gender);

        tvName.setText(preferences.getString(User.USER_NAME, ""));
        tvGender.setText(getResources().getStringArray(R.array.gender)[preferences.getInt(User.USER_GENDER, 0)]);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_profile:
                getSupportActionBar().setTitle("Profile");
                replaceContent(new ProfileFragment());
                break;

            case R.id.nav_home:
                getSupportActionBar().setTitle("Home");
                replaceContent(new Fragment());
                break;

            case R.id.nav_product:
                getSupportActionBar().setTitle("Product");
                replaceContent(new Fragment());
                break;

            case R.id.nav_my_request:
                getSupportActionBar().setTitle("My Request");
                replaceContent(new Fragment());
                break;

            case R.id.nav_logout:
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void replaceContent(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_main_container, fragment)
                .commit();
    }
}
