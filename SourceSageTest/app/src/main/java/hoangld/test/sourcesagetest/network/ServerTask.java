package hoangld.test.sourcesagetest.network;


import android.content.Context;
import android.content.SharedPreferences;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import hoangld.test.sourcesagetest.BuildConfig;
import hoangld.test.sourcesagetest.MainApplication;
import hoangld.test.sourcesagetest.model.User;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServerTask {

    public static String APP_TOKEN = "x-access-token";

    private static ServerTask serverTask = null;

    private Retrofit retrofit;
    private OkHttpClient httpClient;
    private ServerApi servicesCommon;
    private Gson gson;

    private ServerTask() {
        httpClient = provideOkHttpClient();
        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_DOMAIN)
                .addConverterFactory(GsonConverterFactory.create(provideGson()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(httpClient)
                .build();
    }

    private Gson provideGson() {
        if (gson == null) {
            gson = new GsonBuilder().create();
        }
        return gson;
    }

    private OkHttpClient provideOkHttpClient() {
        final OkHttpClient.Builder builder = new OkHttpClient().newBuilder();

        builder.connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor).addNetworkInterceptor(new StethoInterceptor());
        }

        HeaderInterceptor headerInterceptor = new HeaderInterceptor();
        builder.addInterceptor(headerInterceptor);

        return builder.build();
    }

    public static ServerTask getInstance() {

        if (serverTask == null) {
            serverTask = new ServerTask();
        }

        return serverTask;
    }

    public <S> S createService(Class<S> serviceClass) {

        return retrofit.create(serviceClass);
    }

    public ServerApi getServices() {
        if (servicesCommon == null) {
            servicesCommon = createService(ServerApi.class);
        }

        return servicesCommon;
    }

    public static void clearOrResetServerTask() {
        serverTask = null;
    }


    public static class HeaderInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Context context = MainApplication.getAppContext();
            SharedPreferences sharedPreferences = context.getSharedPreferences(User.USER_PREF, Context.MODE_PRIVATE);
            String token = null;

            if (sharedPreferences != null)
                token = sharedPreferences.getString(User.USER_TOKEN, "");

            Request original = chain.request();
            Request request = original.newBuilder()
                    .header(APP_TOKEN, token)
                    .build();
            return chain.proceed(request);
        }

    }
}