package hoangld.test.sourcesagetest.Login;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hoangld.test.sourcesagetest.MainActivity;
import hoangld.test.sourcesagetest.R;
import hoangld.test.sourcesagetest.model.User;
import hoangld.test.sourcesagetest.network.ServerTask;
import hoangld.test.sourcesagetest.register.RegisterActivity;

/**
 * Created by hoangle on 2019-10-12.
 */

public class LoginActivity extends AppCompatActivity implements LoginView {

    @BindView(R.id.activity_login_ed_email)
    EditText edEmail;

    @BindView(R.id.activity_login_ed_password)
    EditText edPass;

    private LoginPresenter presenter;
    private Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        dialog = new ProgressDialog(this);
        ((ProgressDialog) dialog).setMessage("Waiting...");
        dialog.setCancelable(false);
        presenter = new LoginPresenter(this);
    }

    @OnClick(R.id.activity_login_btn_login)
    public void actionBtnLogin() {
        if (TextUtils.isEmpty(edEmail.getText().toString()) || TextUtils.isEmpty(edPass.getText().toString())) {
            Toast.makeText(this, "Email or password is invalid", Toast.LENGTH_SHORT).show();
            return;
        }

        dialog.show();
        presenter.login(edEmail.getText().toString(), edPass.getText().toString());
    }

    @OnClick(R.id.activity_login_btn_sign_up)
    public void actionBtnSignUp() {
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
        finish();
    }

    @Override
    public void onLoginSuccess(JsonObject result) {
        SharedPreferences sharedPreferences = getSharedPreferences(User.USER_PREF, MODE_PRIVATE);
        sharedPreferences.edit().putString(User.USER_TOKEN, result.get(User.USER_TOKEN).getAsString()).apply();

        ServerTask.clearOrResetServerTask();

        presenter.getProfile();
    }

    @Override
    public void onLoginFailed(Throwable err) {
        dialog.dismiss();
        Toast.makeText(this, "Login failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetProfileSuccess(User user) {
        dialog.dismiss();
        SharedPreferences sharedPreferences = getSharedPreferences(User.USER_PREF, MODE_PRIVATE);
        sharedPreferences.edit().putString(User.USER_NAME, user.name)
                .putString(User.USER_EMAIL, user.email)
                .putInt(User.USER_AGE, user.age)
                .putInt(User.USER_GENDER, user.gender)
                .apply();

        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }

    @Override
    public void onGetProfileFailed(Throwable err) {
        dialog.dismiss();
        Toast.makeText(this, "Get profile failed", Toast.LENGTH_SHORT).show();
    }
}
