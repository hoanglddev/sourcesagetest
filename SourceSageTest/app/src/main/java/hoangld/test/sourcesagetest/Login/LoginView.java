package hoangld.test.sourcesagetest.Login;

import com.google.gson.JsonObject;

import hoangld.test.sourcesagetest.model.User;

/**
 * Created by hoangle on 2019-10-12.
 */

public interface LoginView {

    void onLoginSuccess(JsonObject result);

    void onLoginFailed(Throwable err);

    void onGetProfileSuccess(User user);

    void onGetProfileFailed(Throwable err);

}
