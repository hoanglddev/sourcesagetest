package hoangld.test.sourcesagetest.network;

import com.google.gson.JsonObject;


import hoangld.test.sourcesagetest.model.User;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

public interface ServerApi {

    String API_URL = "api";
    String USER_LOGIN = API_URL + "/auth/login";
    String USER_SIGNUP = API_URL + "/auth/register";
    String USER_PROFILE = API_URL + "/auth/me";


    @POST(USER_SIGNUP)
    Observable<JsonObject> signup(@Body User user);

    @POST(USER_LOGIN)
    Observable<JsonObject> login(@Body JsonObject params);

    @GET(USER_PROFILE)
    Observable<User> getProfile();

}
