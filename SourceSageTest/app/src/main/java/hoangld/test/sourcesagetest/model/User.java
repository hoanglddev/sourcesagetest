package hoangld.test.sourcesagetest.model;

import java.io.Serializable;

/**
 * Created by hoangle on 2019-10-12.
 */

public class User implements Serializable {

    public final static String USER_PREF = "user_pref";
    public final static String USER_TOKEN = "token";
    public final static String USER_NAME = "name";
    public final static String USER_EMAIL = "email";
    public final static String USER_AGE = "age";
    public final static String USER_GENDER = "gender";

    public String email;
    public String name;
    public int age;
    public int gender;
    public String password;

}
