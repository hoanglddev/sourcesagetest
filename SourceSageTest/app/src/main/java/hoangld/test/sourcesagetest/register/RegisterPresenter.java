package hoangld.test.sourcesagetest.register;

import hoangld.test.sourcesagetest.model.User;
import hoangld.test.sourcesagetest.network.ServerTask;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by hoangle on 2019-10-12.
 */

public class RegisterPresenter {

    private RegisterView view;

    public RegisterPresenter(RegisterView view) {
        this.view = view;
    }

    public void signup(User user) {
        ServerTask.getInstance().getServices()
                .signup(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    view.onRegisterSucess();
                }, err -> {
                    err.printStackTrace();
                    view.onRegisterFailed(err);
                });
    }

}
