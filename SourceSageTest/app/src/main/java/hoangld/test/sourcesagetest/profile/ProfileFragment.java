package hoangld.test.sourcesagetest.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import hoangld.test.sourcesagetest.R;
import hoangld.test.sourcesagetest.model.User;

/**
 * Created by hoangle on 2019-10-13.
 */

public class ProfileFragment extends Fragment {

    @BindView(R.id.fragment_profile_tv_name)
    TextView tvName;

    @BindView(R.id.fragment_profile_tv_email_address)
    TextView tvEmail;

    @BindView(R.id.fragment_profile_tv_age)
    TextView tvAge;

    @BindView(R.id.fragment_profile_tv_gender)
    TextView tvGender;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SharedPreferences preferences = getActivity().getSharedPreferences(User.USER_PREF, Context.MODE_PRIVATE);

        tvName.setText(preferences.getString(User.USER_NAME, ""));
        tvEmail.setText(preferences.getString(User.USER_EMAIL, ""));
        tvAge.setText(preferences.getInt(User.USER_AGE, 0) + " years");
        tvGender.setText(getResources().getStringArray(R.array.gender)[preferences.getInt(User.USER_GENDER, 0)]);
    }
}
